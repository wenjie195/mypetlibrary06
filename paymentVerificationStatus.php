<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $id = $_SESSION['order_id'];

$conn = connDB();
$id = $_POST['order_id'];
$date = date("Y-m-d");

$productsOrders =  getProductOrders($conn);

if($_SERVER['REQUEST_METHOD'] == 'POST')
{}

$conn->close();
function promptError($msg)
{
    echo '<script>  alert("'.$msg.'");  </script>';
}

function promptSuccess($msg)
{
    echo '<script>  alert("'.$msg.'");   </script>';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Shipping Request | Mypetslibrary" />
<title>Shipping Request | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<?php include 'header.php'; ?>

<!-- <div class="yellow-body padding-from-menu same-padding"> -->
<div class="width100 same-padding menu-distance">

        <h1 class="details-h1" onclick="goBack()">
            <a class="black-white-link2 hover1">
                <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
                <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
                Order Number : #<?php echo $_POST['order_id'];?>
            </a>
        </h1>

    <!-- <form method="POST" action="utilities/updatePaymentVerificationFunction.php"> -->
    <form method="POST" id="paymentVerifiedForm" onsubmit="doPreview(this.submited); return false;">

        <!-- <div class="width100 shipping-div2"> -->
        <div class="width100 scroll-div border-separation">

            <!-- <table class="details-table"> -->
            <table class="width100">
                <tbody>
                <?php
                if(isset($_POST['order_id']))
                {
                    $conn = connDB();
                    //Order
                    $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
                    //OrderProduct
                    $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");

                    if($orderArray != null)
                    {
                    ?>
                        <tr>
                            <td>Name</td>
                            <td>:</td>
                            <td><?php echo $orderArray[0]->getName()?></td>
                        </tr>
                        <tr>
                            <td>Payment Method</td>
                            <td>:</td>
                            <td><?php echo $orderArray[0]->getPaymentMethod()?></td>
                        </tr>
                        <tr>
                            <td>Total Fees</td>
                            <td>:</td>
                            <td><?php echo $orderArray[0]->getTotal()?></td>
                        </tr>
                        <tr>
                            <td>Payment Amount</td>
                            <td>:</td>
                            <td><?php echo $orderArray[0]->getPaymentAmount()?></td>
                        </tr>
                        <tr>
                            <td>Payment Reference</td>
                            <td>:</td>
                            <td><?php echo $orderArray[0]->getPaymentBankReference()?></td>
                        </tr>
                        <tr>
                            <td>Payment Date and Time</td>
                            <td>:</td>
                            <td><?php echo $orderArray[0]->getPaymentDate();?>&nbsp;<?php echo $orderArray[0]->getPaymentTime()?></td>
                        </tr>
                        <tr>
                        	<td>Receipt</td>
                            <td>:</td>
                            <td>
                                <?php
                                $withdrawalNumberNew  = $orderArray[0]->getReceipt();
                                // Include the database configuration file

                                // Get images from the database
                                $query = $conn->query("SELECT receipt FROM orders WHERE id = '$id'");
                                if($query->num_rows > 0)
                                {
                                    while($row = $query->fetch_assoc())
                                    {
                                        $imageURL = 'receipt/'.$row["receipt"];
                                        if ($row["receipt"] != null)
                                        {
                                        ?>
                                            <a class="img" href="<?php echo $imageURL;?>" data-fancybox="images-preview1" class="image-popout">
                                                <img src="<?php echo $imageURL; ?>" class="details-img receipt-img">
                                            </a>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <p class="b">No Receipt Uploaded By User.</p>
                                        <?php
                                        }
                                    }
                                }
                                else
                                {
                                ?>
                                    <p>No image(s) found...</p>
                                <?php
                                }
                                ?>
                            </td>
                        <tr>
                        <?php
                    }
                }
                else
                {}
                $conn->close();
                ?>
                </tbody>
            </table>

            <div class="dual-input">
                <p>ISSUE DATE</p>
                <!-- <input class="input-name clean input-textarea admin-input" type="date" id="shipping_date" name="shipping_date" value="<?php echo $date; ?>" required> -->
                <input class="input-name clean input-textarea admin-input" type="date" value="<?php echo $date; ?>" name="shipping_date" id="shipping_date" required>
            </div>
        </div>

        <!-- <div class="clear"></div> -->

        <input class="input-name clean input-textarea admin-input" type="hidden" id="order_id" name="order_id" value="<?php echo $orderArray[0]->getId()?>">
        <input class="input-name clean input-textarea admin-input" type="hidden" id="user_uid" name="user_uid" value="<?php echo $orderArray[0]->getUid()?>">
        <input class="input-name clean input-textarea admin-input" type="hidden" id="subtotal_value" name="subtotal_value" value="<?php echo $orderArray[0]->getSubtotal()?>">
    

        <div class="width100 overflow text-center">   
            <input onclick="this.form.submited=this.value;"  type="submit" name="REJECT" value="REJECT" class="red-btn white-text clean2 edit-1-btn margin-auto bottom-reject">
            <input onclick="this.form.submited=this.value;"  type="submit" name="ACCEPTED" value="ACCEPTED" class="green-button white-text clean2 edit-1-btn margin-auto" >
        </div>

    </form>

</div>

<?php include 'js.php'; ?>


<script>
function goBack() 
{
window.history.back();
}
</script>


<script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType){
            case 'ACCEPTED':
                form=document.getElementById('paymentVerifiedForm');
                // form.action='shippingRefund.php';
                form.action='utilities/updatePaymentVerificationAcceptedFunction.php';
                form.submit();
            break;
            case 'REJECT':
                form=document.getElementById('paymentVerifiedForm');
                form.action='utilities/updatePaymentVerificationRejectFunction.php';
                form.submit();
            break;
        }

    }
</script>

</body>
</html>