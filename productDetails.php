<?php
// if (session_id() == "")
// {
//   session_start();
// }

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/Kitten.php';
// require_once dirname(__FILE__) . '/classes/Puppy.php';
//require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Variation.php';
//require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    header('Location: ./cart.php');
}

if(isset($_GET['id'])){
    $referrerUidLink = $_GET['id'];
}
else{
    echo "error";
}

//$puppies = getPuppy($conn, "WHERE status = 'Available' ");
// $kittens = getKitten($conn, "WHERE status = 'Available' ");
//$products = getProduct($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");
$variation = getVariation($conn);

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($variation,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($variation);
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pedigree Dentastix Dog Treats | Mypetslibrary" />
<title>Pedigree Dentastix Dog Treats | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet.Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet. Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 menu-distance3 same-padding min-height2">

    <form method="POST">
        <div >
        <?php
        if(isset($_GET['id'])){
            $referrerUidLink = $_GET['id'];
            echo "tt";
            echo "tt";
            echo "tt";
            echo $referrerUidLink;
        }
        else{
            echo "error";
            echo "error";
            echo "error";
            echo "error";
        }
        ?>
            <!-- <//?php echo $productListHtml; ?> -->
        </div>

        <div class="clear"></div>  
        <div class="width100 text-center">                                                                         
            <button class="green-button checkout-btn clean">Add to Cart</button>
        </div>

    </form>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<div class="sticky-distance2 width100"></div>

<div class="sticky-call-div">
<table class="width100 sticky-table">
	<tbody>
    	<tr>
        	<td>
                <a  href="tel:+60383190000" class="text-center clean transparent-button same-text" >
                    Call
                </a>
        	</td>
            <td>
                <a onclick="window.open(this.href); return false;" href="https://api.whatsapp.com/send?phone=601159118132"  class="text-center clean transparent-button same-text">
                    Whatsapp
                </a>
        	</td>
    	</tr>
    </tbody>    
</table>   
</div>

<?php include 'stickyFooter.php'; ?>

<style>
.social-dropdown {
    width: 360px;
}
</style>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>

</body>
</html>
