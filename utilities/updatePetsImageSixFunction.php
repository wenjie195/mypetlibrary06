<?php 
//require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST['uid']);
    // $imageOne = $_POST['image_one'];

    $imgOne = $_FILES['image_one']['name'];
    if($imgOne != "")
    {
        $imageOne = $timestamp.$uid.$_FILES['image_one']['name'];
        $target_dir = "../uploads/";
        $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
        // Select file type
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Valid file extensions
        $extensions_arr = array("jpg","jpeg","png","gif");
        if( in_array($imageFileType,$extensions_arr) )
        {
        move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
        }
    }

    //   FOR DEBUGGING 
    //  echo "<br>";
    // echo $uid."<br>";
    // echo $imageOne."<br>";

}

if(isset($_POST['editImage']))
{   
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    //echo "save to database";
    if($imageOne)
    {
        array_push($tableName,"image_six");
        array_push($tableValue,$imageOne);
        $stringType .=  "s";
    }

    array_push($tableValue,$uid);
    $stringType .=  "s";
    $updateProductDetails = updateDynamicData($conn,"puppy"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    if($updateProductDetails)
    { 
        if(isset($_POST['editImage']))
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";

            //echo "save to database";
            if($imageOne)
            {
                array_push($tableName,"image_six");
                array_push($tableValue,$imageOne);
                $stringType .=  "s";
            }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $updatePetsDetails = updateDynamicData($conn,"pet_details"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($updatePetsDetails)
            {
                if(isset($_SESSION['defaultImage']) && isset($_SESSION['imageSix']))
                {
                    if($_SESSION['defaultImage'] == $_SESSION['imageSix'])
                    {
                        if(isset($_POST['editImage']))
                        {   
                            $tableName = array();
                            $tableValue =  array();
                            $stringType =  "";

                            //echo "save to database";
                            if($imageOne)
                            {
                                array_push($tableName,"default_image");
                                array_push($tableValue,$imageOne);
                                $stringType .=  "s";
                            }

                            array_push($tableValue,$uid);
                            $stringType .=  "s";
                            $updateDefaultDetails = updateDynamicData($conn,"puppy"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                            if($updateDefaultDetails){
                                $_SESSION['messageType'] = 1;
                                echo "<script>alert('Data Updated and Stored !');window.location='../allPuppies.php'</script>"; 
                            }
                            else
                            {      
                                $_SESSION['messageType'] = 1;
                                echo "<script>alert('Fail to Update Data !');window.location='../allPuppies.php'</script>"; 
                            }
                        }
                    }else
                    {
                        $_SESSION['messageType'] = 1;
                        echo "<script>alert('Data Updated and Stored !');window.location='../allPuppies.php'</script>"; 
                    }
                }
            }
            else
            {      
                $_SESSION['messageType'] = 1;
                echo "<script>alert('Fail to Update Data !');window.location='../allPuppies.php'</script>"; 
            }
        }
    }
    else
    {      
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Fail to Update Data !');window.location='../allPuppies.php'</script>"; 
    }
}
else
{
    header('Location: ../index.php');
    // $_SESSION['messageType'] = 1;
    //header('Location: ../editProfile.php?type=1');
}
 ?>