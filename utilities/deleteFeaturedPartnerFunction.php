<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Seller.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $sellerUid = rewrite($_POST["seller_uid"]);
    $status = "Banned";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $sellerUid."<br>";

    if(isset($_POST['seller_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"account_status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }    

        array_push($tableValue,$sellerUid);
        $stringType .=  "s";
        $deleteSeller = updateDynamicData($conn,"seller"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($deleteSeller)
        {
            // echo "credit card deleted";
            $_SESSION['messageType'] = 1;
            header('Location: ../featuredPartners.php?type=1');
        }
        else
        {
            // echo "fail";
            $_SESSION['messageType'] = 1;
            header('Location: ../featuredPartners.php?type=2');
        }
    }
    else
    {
        // echo "error";
        $_SESSION['messageType'] = 1;
        header('Location: ../featuredPartners.php?type=3');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>