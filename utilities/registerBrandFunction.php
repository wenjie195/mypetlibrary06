<?php

if (session_id() == ""){
     session_start();
 }
 
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Brand.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewBrand($conn,$name,$status)
{
     if(insertDynamicData($conn,"brand",array("name","status"),
          array($name,$status),"ss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $name = rewrite($_POST['register_name']);
     $status = rewrite($_POST['register_status']);

     //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $name."<br>";
    //  echo $status."<br>";

        if($name && $status)
        {
            if(registerNewBrand($conn,$name,$status))
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../addBrand.php?type=1');
                //echo "done register";   
                // $_SESSION['uid'] = $uid;
                // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../addBrand.php?type=4');
                //echo "done failed";
            }
        }
        else if(!$name)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addBrand.php?type=2');
            //echo "fail to register breed";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
        else if(!$status)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addBrand.php?type=3');
            //echo "fail to register breed";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
}
else 
{
     header('Location: ../index.php');
}

?>