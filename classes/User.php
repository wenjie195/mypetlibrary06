<?php  
class User {
    /* Member variables */
    var $id,$uid,$name,$email,$country,$phoneNo,$tac,$password,$salt,$userType,$fbId,$birthday,$gender,$accountStatus,$receiverName,$receiverContactNo,$shippingState,
            $shippingArea,$shippingPostalCode,$shippingAddress,$bankName,$bankAccountHolder,$bankAccountNo,$nameOnCard,$cardNo,$cardType,$expiryDate,$ccv,$postalCode,
                $billingAddress,$profilePic,$points,$favPuppy,$favKitten,$favReptile,$favProduct,$emailVerificationCode,$isEmailVerified,$isPhoneVerified,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getPhoneNo()
    {
        return $this->phoneNo;
    }

    /**
     * @param mixed $phoneNo
     */
    public function setPhoneNo($phoneNo)
    {
        $this->phoneNo = $phoneNo;
    }

    /**
     * @return mixed
     */
    public function getTac()
    {
        return $this->tac;
    }

    /**
     * @param mixed $tac
     */
    public function setTac($tac)
    {
        $this->tac = $tac;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getFbId()
    {
        return $this->fbId;
    }

    /**
     * @param mixed $fbId
     */
    public function setFbId($fbId)
    {
        $this->fbId = $fbId;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getAccountStatus()
    {
        return $this->accountStatus;
    }

    /**
     * @param mixed $accountStatus
     */
    public function setAccountStatus($accountStatus)
    {
        $this->accountStatus = $accountStatus;
    }

    /**
     * @return mixed
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }

    /**
     * @param mixed $receiverName
     */
    public function setReceiverName($receiverName)
    {
        $this->receiverName = $receiverName;
    }

    /**
     * @return mixed
     */
    public function getReceiverContactNo()
    {
        return $this->receiverContactNo;
    }

    /**
     * @param mixed $receiverContactNo
     */
    public function setReceiverContactNo($receiverContactNo)
    {
        $this->receiverContactNo = $receiverContactNo;
    }

    /**
     * @return mixed
     */
    public function getShippingState()
    {
        return $this->shippingState;
    }

    /**
     * @param mixed $shippingState
     */
    public function setShippingState($shippingState)
    {
        $this->shippingState = $shippingState;
    }

    /**
     * @return mixed
     */
    public function getShippingArea()
    {
        return $this->shippingArea;
    }

    /**
     * @param mixed $shippingArea
     */
    public function setShippingArea($shippingArea)
    {
        $this->shippingArea = $shippingArea;
    }

    /**
     * @return mixed
     */
    public function getShippingPostalCode()
    {
        return $this->shippingPostalCode;
    }

    /**
     * @param mixed $shippingPostalCode
     */
    public function setShippingPostalCode($shippingPostalCode)
    {
        $this->shippingPostalCode = $shippingPostalCode;
    }

    /**
     * @return mixed
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * @param mixed $shippingAddress
     */
    public function setShippingAddress($shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

/**
     * @return mixed
     */
    public function getBankAccountHolder()
    {
        return $this->bankAccountHolder;
    }

    /**
     * @param mixed $bankAccountHolder
     */
    public function setBankAccountHolder($bankAccountHolder)
    {
        $this->bankAccountHolder = $bankAccountHolder;
    }

    /**
     * @return mixed
     */
    public function getBankAccountNo()
    {
        return $this->bankAccountNo;
    }

    /**
     * @param mixed $bankAccountNo
     */
    public function setBankAccountNo($bankAccountNo)
    {
        $this->bankAccountNo = $bankAccountNo;
    }

    /**
     * @return mixed
     */
    public function getNameOnCard()
    {
        return $this->nameOnCard;
    }

    /**
     * @param mixed $nameOnCard
     */
    public function setNameOnCard($nameOnCard)
    {
        $this->nameOnCard = $nameOnCard;
    }

    /**
     * @return mixed
     */
    public function getCardNo()
    {
        return $this->cardNo;
    }

    /**
     * @param mixed $cardNo
     */
    public function setCardNo($cardNo)
    {
        $this->cardNo = $cardNo;
    }

    /**
     * @return mixed
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param mixed $cardType
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
    }

    /**
     * @return mixed
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param mixed $expiryDate
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return mixed
     */
    public function getCcv()
    {
        return $this->ccv;
    }

    /**
     * @param mixed $ccv
     */
    public function setCcv($ccv)
    {
        $this->ccv = $ccv;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return mixed
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param mixed $billingAddress
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return mixed
     */
    public function getProfilePic()
    {
        return $this->profilePic;
    }

    /**
     * @param mixed $profilePic
     */
    public function setProfilePic($profilePic)
    {
        $this->profilePic = $profilePic;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * @return mixed
     */
    public function getFavPuppy()
    {
        return $this->favPuppy;
    }

    /**
     * @param mixed $favPuppy
     */
    public function setFavPuppy($favPuppy)
    {
        $this->favPuppy = $favPuppy;
    }

    /**
     * @return mixed
     */
    public function getFavKitten()
    {
        return $this->favKitten;
    }

    /**
     * @param mixed $favKitten
     */
    public function setFavKitten($favKitten)
    {
        $this->favKitten = $favKitten;
    }

    /**
     * @return mixed
     */
    public function getFavReptile()
    {
        return $this->favReptile;
    }

    /**
     * @param mixed $favReptile
     */
    public function setFavReptile($favReptile)
    {
        $this->favReptile = $favReptile;
    }

    /**
     * @return mixed
     */
    public function getFavProduct()
    {
        return $this->favProduct;
    }

    /**
     * @param mixed $favProduct
     */
    public function setFavProduct($favProduct)
    {
        $this->favProduct = $favProduct;
    }

    /**
     * @return mixed
     */
    public function getEmailVerificationCode()
    {
        return $this->emailVerificationCode;
    }

    /**
     * @param mixed $emailVerificationCode
     */
    public function setEmailVerificationCode($emailVerificationCode)
    {
        $this->emailVerificationCode = $emailVerificationCode;
    }

    /**
     * @return mixed
     */
    public function getisEmailVerified()
    {
        return $this->isEmailVerified;
    }

    /**
     * @param mixed $isEmailVerified
     */
    public function setIsEmailVerified($isEmailVerified)
    {
        $this->isEmailVerified = $isEmailVerified;
    }

    /**
     * @return mixed
     */
    public function getisPhoneVerified()
    {
        return $this->isPhoneVerified;
    }

    /**
     * @param mixed $isPhoneVerified
     */
    public function setIsPhoneVerified($isPhoneVerified)
    {
        $this->isPhoneVerified = $isPhoneVerified;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","name","email","country","phone_no","tac","password","salt","user_type","fb_id","birthday","gender","account_status","receiver_name",
                            "receiver_contact_no","shipping_state","shipping_area","shipping_postal_code","shipping_address","bank_name","bank_account_holder","bank_account_no",
                            "name_on_card","card_no","card_type","expiry_date","ccv","postal_code","billing_address","profile_pic","points","favorite_puppy","favorite_kitten",
                            "favorite_reptile","favorite_product","email_verification_code","is_email_verified","is_phone_verified","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$name,$email,$country,$phoneNo,$tac,$password,$salt,$userType,$fbId,$birthday,$gender,$accountStatus,$receiverName,$receiverContactNo,
                            $shippingState,$shippingArea,$shippingPostalCode,$shippingAddress,$bankName,$bankAccountHolder,$bankAccountNo,$nameOnCard,$cardNo,$cardType,
                            $expiryDate,$ccv,$postalCode,$billingAddress,$profilePic,$points,$favPuppy,$favKitten,$favReptile,$favProduct,$emailVerificationCode,$isEmailVerified,
                            $isPhoneVerified,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setId($id);
            $user->setUid($uid);
            $user->setName($name);
            $user->setEmail($email);
            $user->setCountry($country);
            $user->setPhoneNo($phoneNo);
            $user->setTac($tac);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setUserType($userType);
            $user->setFbId($fbId);
            $user->setBirthday($birthday);
            $user->setGender($gender);
            $user->setAccountStatus($accountStatus);
            $user->setReceiverName($receiverName);
            $user->setReceiverContactNo($receiverContactNo);
            $user->setShippingState($shippingState);
            $user->setShippingArea($shippingArea);
            $user->setShippingPostalCode($shippingPostalCode);
            $user->setShippingAddress($shippingAddress);
            $user->setBankName($bankName);
            $user->setBankAccountHolder($bankAccountHolder);
            $user->setBankAccountNo($bankAccountNo);
            $user->setNameOnCard($nameOnCard);
            $user->setCardNo($cardNo);
            $user->setCardType($cardType);
            $user->setExpiryDate($expiryDate);
            $user->setCcv($ccv);
            $user->setPostalCode($postalCode);
            $user->setBillingAddress($billingAddress);

            $user->setProfilePic($profilePic);

            $user->setPoints($points);

            $user->setFavPuppy($favPuppy);
            $user->setFavKitten($favKitten);
            $user->setFavReptile($favReptile);
            $user->setFavProduct($favProduct);

            $user->setEmailVerificationCode($emailVerificationCode);
            $user->setIsEmailVerified($isEmailVerified);
            $user->setIsPhoneVerified($isPhoneVerified);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
