<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$productList = getProduct($conn);


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Malaysia Pet Food Toy Product | Mypetslibrary" />
<title>Malaysia Pet Food Toy Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="pet food, pet grooming, pet shampoo, toy for pet, pet product, Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
	<div class="fix-filter width100  small-padding overflow product-filter-white-div">
        <h1 class="green-text user-title left-align-title product-filter-h1">Product</h1>
        <a class="open-productfilter filter-a green-a product-filter-a">Filter</a>
        <div class="filter-div product-filter-div">
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Favourite</button>
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Top Sale</button>
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Free Gift</button> 
            <button class="transparent-button filter-btn clean grey-to-green product-btn">Price</button>
            
        </div>
        
    </div>

	<div class="clear"></div> 
       
<div class="width100 small-padding overflow min-height-with-filter filter-distance product-filter-distance">

<?php
    if($productList)
    {               
        for($cnt = 0;$cnt < count($productList) ;$cnt++)
        {?>

            <div class="width103 product-big-div">
                <form action="productDetails.php?" method="POST">
                    <button class="shadow-white-box featured four-box-size" type="submit" name="product_id" value="<?php echo $productList[$cnt]->getId();?>">
                        <div class="width100 white-bg">
                            <img src="<?php echo './uploads/'.$productList[$cnt]->getImageOne();?>" alt="<?php echo $productList[$cnt]->getName();?>" title="<?php echo $productList[$cnt]->getName();?>" class="width100 two-border-radius">
                        </div>
                        <div class="width100 product-details-div">
                                <p class="width100 text-overflow slider-product-name"><?php echo $productList[$cnt]->getName();?></p>
                                <p class="product-price-width text-overflow slider-product-price left-price">RM <?php echo $productList[$cnt]->getVariationOnePrice();?>.00</p>
                                <p class="slider-product-price right-like hover1">
                                    <img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                                    <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                                </p>
                                <div class="clear"></div>
                                <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                                <p class="right-sold"><?php echo $productList[$cnt]->getVariationOneStock();?><span class="sold-color"><?php echo $productList[$cnt]->getStatus();?></span></p>
                        </div>
                    </button>
                </form>                              
            </div>
        <?php
        }
    }
    ?>

</div>

<div class="clear"></div>
<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.product-a .hover1a{
		display:none !important;}
	.product-a .hover1b{
		display:inline-block !important;}	
</style>
<?php include 'js1.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>