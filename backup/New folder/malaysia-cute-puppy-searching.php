<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Reptile.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $puppies = getPuppy($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");
// // $puppies = getPuppy($conn);

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
      $gender = rewrite($_POST["gender"]);
      $petType = rewrite($_POST["pet_type"]);

      if($petType == 'Puppy')
      {
            // $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND gender = '$gender' AND status = 'Available' ORDER BY date_created DESC ");

            if($gender == 'Male')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND gender = '$gender' AND status = 'Available' ORDER BY date_created DESC ");
            }
            elseif($gender == 'Female')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND gender = '$gender' AND status = 'Available' ORDER BY date_created DESC ");
            }
            elseif($gender == 'all_gender')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND status = 'Available' ORDER BY date_created DESC ");
            }

      }
      elseif($petType == 'Kitten')
      {
            // $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND gender = '$gender' AND status = 'Available' ORDER BY date_created DESC ");

            if($gender == 'Male')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND gender = '$gender' AND status = 'Available' ORDER BY date_created DESC ");
            }
            elseif($gender == 'Female')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND gender = '$gender' AND status = 'Available' ORDER BY date_created DESC ");
            }
            elseif($gender == 'all_gender')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND status = 'Available' ORDER BY date_created DESC ");
            }

      }
      elseif($petType == 'Reptile')
      {
            // $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND gender = '$gender' AND status = 'Available' ORDER BY date_created DESC ");

            if($gender == 'Male')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND gender = '$gender' AND status = 'Available' ORDER BY date_created DESC ");
            }
            elseif($gender == 'Female')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND gender = '$gender' AND status = 'Available' ORDER BY date_created DESC ");
            }
            elseif($gender == 'all_gender')
            {
                  $petsDet = getPetsDetails($conn, "WHERE type = '$petType' AND status = 'Available' ORDER BY date_created DESC ");
            }

      }
      elseif($petType == 'puppy_price')
      {
            // $petGender = 'Male';
            $searchAmount = rewrite($_POST["amount"]);
            if($searchAmount == '')
            {
                  $amount = 'ASC';
            }
            else
            {
                  $amount = rewrite($_POST["amount"]);
            }

            $petsDet = getPetsDetails($conn, "WHERE type = 'Puppy' AND status = 'Available' ORDER BY price+0 $amount ");
      }
      elseif($petType == 'kitten_price')
      {
            // $petGender = 'Male';
            $searchAmount = rewrite($_POST["amount"]);
            if($searchAmount == '')
            {
                  $amount = 'ASC';
            }
            else
            {
                  $amount = rewrite($_POST["amount"]);
            }

            $petsDet = getPetsDetails($conn, "WHERE type = 'Kitten' AND status = 'Available' ORDER BY price+0 $amount ");
      }
      elseif($petType == 'reptile_price')
      {
            // $petGender = 'Male';
            $searchAmount = rewrite($_POST["amount"]);
            if($searchAmount == '')
            {
                  $amount = 'ASC';
            }
            else
            {
                  $amount = rewrite($_POST["amount"]);
            }

            $petsDet = getPetsDetails($conn, "WHERE type = 'Reptile' AND status = 'Available' ORDER BY price+0 $amount ");
      }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Puppy/Dog On Sale in Malaysia | Mypetslibrary" />
<title>Puppy/Dog On Sale in Malaysia | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
      
<div class="fix-filter width100 small-padding overflow">
      <h1 class="green-text user-title left-align-title">Puppies</h1>

            <div class="filter-div">
                  <form method="POST" action="malaysia-cute-puppy-searching.php">
                        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $petType;?>" name="pet_type" id="pet_type" readonly>
                        <button class="transparent-button filter-btn clean opacity-hover" value="Male" name="gender" id="gender">
                              <img src="img/male.png" alt="Male" title="Male" class="filter-icon">
                        </button>
                  </form>
                  <form method="POST" action="malaysia-cute-puppy-searching.php">
                        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $petType;?>" name="pet_type" id="pet_type" readonly>
                        <button class="transparent-button filter-btn clean opacity-hover" value="Female" name="gender" id="gender">
                              <img src="img/female.png" alt="Female" title="Female" class="filter-icon">
                        </button>
                  </form>
                  <form method="POST" action="malaysia-cute-puppy-searching.php">
                        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $petType;?>" name="pet_type" id="pet_type" readonly>
                        <button class="transparent-button filter-btn clean opacity-hover" value="all_gender" name="gender" id="gender">
                              <img src="img/both-gender.png" alt="Both Gender" title="Both Gender" class="filter-icon">
                        </button> 
                  </form>

                  <form method="POST" action="malaysia-cute-puppy-searching.php">
                        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $petType ;?>" name="pet_type" id="pet_type" readonly>

                        <input class="clean pop-input no-bg-input" type="hidden" value="DESC" name="amount" id="amount" readonly>
                        <button class="transparent-button filter-btn clean opacity-hover" value="all_gender" name="gender" id="gender">
                              <img src="img/price-filter.png" alt="Price" title="Price" class="filter-icon">
                        </button>
                  </form>

                  <a class="open-filter filter-a green-a">Filter</a>
            </div>
      
</div>

      <div class="clear"></div>    

<div class="width100 small-padding overflow min-height-with-filter filter-distance">
	<div class="width103">      

            <?php
            $conn = connDB();
            if($petsDet)
            {
                  for($cnt = 0;$cnt < count($petsDet) ;$cnt++)
                  {
                  ?>

                  <a href='puppyDetails.php?id=<?php echo $petsDet[$cnt]->getUid();?>'>
                        <div class="shadow-white-box featured four-box-size">
                              <div class="width100 white-bg">
                                    <img src="uploads/<?php echo $petsDet[$cnt]->getImageOne();?>" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                              </div>
                              <div class="width100 product-details-div">
                                    <p class="width100 text-overflow slider-product-name"><?php echo $petsDet[$cnt]->getName();?></p>
                                    <p class="slider-product-price">
                                          RM<?php $length = strlen($petsDet[$cnt]->getPrice());?>
                                          <?php echo substr($petsDet[$cnt]->getPrice(),0,1);?>
                                          <?php 
                                          if($length == 2)
                                          {
                                          echo "X";
                                          }
                                          elseif($length == 3)
                                          {
                                          echo "XX";
                                          }
                                          elseif($length == 4)
                                          {
                                          echo "XXX";
                                          }
                                          elseif($length == 5)
                                          {
                                          echo "XXXX";
                                          }
                                          elseif($length == 6)
                                          {
                                          echo "XXXXX";
                                          }
                                          elseif($length == 7)
                                          {
                                          echo "XXXXXX";
                                          }
                                          ?>
                                    </p>
                                    <p class="width100 text-overflow slider-location"><?php echo $petsDet[$cnt]->getGender();?></p>
                              </div>
                        </div>
                  </a> 

                  <?php
                  }
                  ?>
            <?php
            }
            $conn->close();
            ?>

    </div>
</div>

<div class="clear"></div>

<style>
      .animated.slideUp{
            animation:none !important;}
      .animated{
            animation:none !important;}
      .puppy-a .hover1a{
            display:none !important;}
      .puppy-a .hover1b{
            display:inline-block !important;}	
</style>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>