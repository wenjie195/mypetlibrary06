                            <form action="utilities/editDefaultFunction.php" method="post">
                                <table id="myTableD" class="profile-td-td">
                                	<tr>
                                    <?php 
                                        if ($puppyDetails[0]->getImageOne()) {
                                            ?>
                                            <td><img class="image-select pet-photo-preview" src="<?php echo $imageOne; ?>"></td>
                                            <?php
                                        }
                                        if ($puppyDetails[0]->getImageTwo()) {
                                            ?>
                                            <td><img class="image-select pet-photo-preview" src="<?php echo $imageTwo; ?>"></td>
                                            <?php
                                        }
                                        if ($puppyDetails[0]->getImageThree()) {
                                            ?>
                                            <td><img class="image-select pet-photo-preview" src="<?php echo $imageThree; ?>"></td>
                                            <?php
                                        }
                                        if ($puppyDetails[0]->getImageFour()) {
                                            ?>
                                            <td><img class="image-select pet-photo-preview" src="<?php echo $imageFour; ?>"></td>
                                            <?php
                                        }
                                        if ($puppyDetails[0]->getImageFive()) {
                                            ?>
                                            <td><img class="image-select pet-photo-preview" src="<?php echo $imageFive; ?>"></td>
                                            <?php
                                        }
                                        if ($puppyDetails[0]->getImageSix()) {
                                            ?>
                                            <td><img class="image-select pet-photo-preview" src="<?php echo $imageSix; ?>"></td>
                                            <?php
                                        }?>
                                    </tr>
                                    <tr>
                                    <?php
                                    if ($puppyDetails[0]->getImageOne()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <td><input type="radio" name="default_image" class="pet-photo-preview" id="1" value= "<?php echo $puppyDetails[0]->getImageOne() ?>"
                                            <?php 
                                            if($imageOne==$defaultimage) {
                                                echo "checked";
                                            }
                                            ?>/>
                                        </td>
                                        <?php
                                    }
                                    if ($puppyDetails[0]->getImageTwo()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <td><input type="radio" name="default_image" class="pet-photo-preview" id="2" value="<?php echo $puppyDetails[0]->getImageTwo() ?>"
                                            <?php 
                                                if($imageTwo==$defaultimage) {
                                                    echo "checked";
                                                }
                                            ?>/>
                                        </td>
                                        <?php
                                    }
                                    if ($puppyDetails[0]->getImageThree()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <td><input type="radio" name="default_image" class="pet-photo-preview" id="3" value="<?php echo $puppyDetails[0]->getImageThree() ?>"
                                            <?php 
                                                if($imageThree==$defaultimage) {
                                                    echo "checked";
                                                }
                                            ?>/>
                                        </td>
                                        <?php
                                    }
                                    if ($puppyDetails[0]->getImageFour()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <td><input type="radio" name="default_image" class="pet-photo-preview" id="4" value="<?php echo $puppyDetails[0]->getImageFour() ?>"
                                            <?php 
                                                if($imageFour==$defaultimage) {
                                                    echo "checked";
                                                }
                                            ?>/>
                                        </td>
                                        <?php
                                    }
                                    if ($puppyDetails[0]->getImageFive()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <td><input type="radio" name="default_image" class="pet-photo-preview" id="5" value="<?php echo $puppyDetails[0]->getImageFive() ?>"
                                            <?php 
                                                if($imageFive==$defaultimage) {
                                                    echo "checked";
                                                }
                                            ?>/>
                                        </td>
                                        <?php
                                    }
                                    if ($puppyDetails[0]->getImageSix()) {
                                        ?>
                                        <input type="hidden" id="uid" name="uid" value="<?php echo $puppyDetails[0]->getUid() ?>">
                                        <td><input type="radio" name="default_image" class="pet-photo-preview" id="6" value="<?php echo $puppyDetails[0]->getImageSix() ?>"
                                            <?php 
                                                if($imageSix==$defaultimage) {
                                                    echo "checked";
                                                }
                                            ?>/>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    </tr>
                                </table>
                                <div class="clear"></div>  
                                <div class="width100 overflow text-center">  
                                    <button type="submit" class="green-button white-text clean2 edit-1-btn margin-auto" name="defaultImageSubmit" id="defaultImageSubmit">Change Profile Image</button>
                                </div>
                            </form>