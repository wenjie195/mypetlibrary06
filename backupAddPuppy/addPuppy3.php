<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Color.php';
require_once dirname(__FILE__) . '/classes/Breed.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$sellerDetails = getSeller($conn);

$colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(1),"s");
$breedDetails = getBreed($conn," WHERE type = ? ",array("type"),array(1),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add a Puppy | Mypetslibrary" />
<title>Add a Puppy | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->

<link rel="stylesheet" type="text/css" href="css/filepond-plugin-image-preview.css">
<link rel="stylesheet" type="text/css" href="css/filepond.css">
<link rel="stylesheet" href="bootstrap.min.css" />
<link rel="stylesheet" href="croppie.css" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<script src="js/filepond-plugin-image-preview.js" type="text/javascript"></script>
<script src="js/filepond-plugin-file-encode.js" type="text/javascript"></script>
<script src="js/filepond.js" type="text/javascript"></script>  
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add a Puppy</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
        <form action="utilities/adminRegisterPuppyFunction.php" method="POST" enctype="multipart/form-data">
         <!--
         <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Color <a href="puppyColor.php" class="green-a">(Add New Color Here)*</a></p>
            <select class="clean input-name admin-input" value="<?php echo $colorDetails[0]->getName();?>" name="register_color" id="register_color" required>
                <option value="">Please Select a Color</option>
                <?php
                for ($cntPro=0; $cntPro <count($colorDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $colorDetails[$cntPro]->getName(); ?>"> 
                        <?php echo $colorDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>       
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Pet Breed <a href="puppyBreed.php" class="green-a">(Add New Breed Here)*</a></p>
        	<select class="clean input-name admin-input" value="<?php echo $breedDetails[0]->getName();?>" name="register_breed" id="register_breed" required>
                <option value="">Please Select a Breed</option>
                <?php
                for ($cntPro=0; $cntPro <count($breedDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $breedDetails[$cntPro]->getName(); ?>"> 
                        <?php echo $breedDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>       
        </div>

		<div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">SKU*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="SKU" name="register_sku" id="register_sku" required> 
        </div>

        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Pet Price (RM)*</p>
          <input class="input-name clean input-textarea admin-input" type="number" placeholder="Pet Price (RM)"  name="register_price" id="register_price" required>
        </div>   


        <div class="clear"></div>  
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Age*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Pet Age"  name="register_age" id="register_age" required>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Vaccinated Status*</p>
            <select class="clean input-name admin-input" type="text" name="register_vaccinated" id="register_vaccinated" required>
      
              
                <option value="1st Vaccination Done" name="First Vaccination Done">1st Vaccination Done</option>
                <option value="2nd Vaccination Done" name="Second Vaccination Done">2nd Vaccination Done</option>
                <option value="3rd Vaccination Done" name="Third Vaccination Done">3rd Vaccination Done</option>
                <option value="No" name="No">No</option>
            </select> 

        </div>      

      <div class="clear"></div>
    
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Dewormed Status*</p>
            <select class="clean input-name admin-input" type="text" name="register_dewormed" id="register_dewormed" required>
         
                <option value="Yes" name="Yes">Yes</option>
                <option value="No" name="No">No</option>
            </select> 
        </div>

        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Gender*</p>
            <select class="clean input-name admin-input" type="text" name="register_gender" id="register_gender" required>
                <option value="" name=" ">Select a Gender</option>
                <option value="Male" name="Male">Male</option>
                <option value="Female" name="Female">Female</option>
            </select> 
        </div>  
        
        <div class="clear"></div>

        <input class="input-name clean input-textarea admin-input" type="hidden" value="Pending"  name="register_status" id="register_status" readonly>     
    
        <div class="clear"></div>

        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Pet Size*</p>
            <select class="clean input-name admin-input" type="text" name="register_size" id="register_size" required>
                <option value="" name=" ">Select a Size</option>
                <option value="Small" name="Small">Small</option>
                <option value="Medium" name="Medium">Medium</option>
                <option value="Large" name="Large">Large</option>
            </select> 
        </div> 

        <div class="dual-input second-dual-input">
            <p class="input-top-p admin-top-p">Seller*</p> 
        	<select class="clean input-name admin-input" value="<?php echo $sellerDetails[0]->getCompanyName();?>"name="register_seller" id="register_seller" required>
                <option value="">Please Select a Seller</option>
                <?php
                for ($cntPro=0; $cntPro <count($sellerDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $sellerDetails[$cntPro]->getCompanyName(); ?>"> 
                        <?php echo $sellerDetails[$cntPro]->getCompanyName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>   
        </div>         

        <div class="clear"></div>

        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Details</p>
          <input class="input-name clean input-textarea admin-input" type="text" placeholder="Details 1" name="register_details" id="register_details">    
          <input class="input-name clean input-textarea admin-input" type="text" placeholder="Details 2" name="register_details_two" id="register_details_two">    
          <input class="input-name clean input-textarea admin-input" type="text" placeholder="Details 3" name="register_details_three" id="register_details_three">    
          <input class="input-name clean input-textarea admin-input" type="text" placeholder="Details 4" name="register_details_four" id="register_details_four">           
        </div>
      
        <div class="clear"></div>

        <div class="width100 overflow">
        	<img src="img/vimeo-tutorial.png" class="vimeo-tutorial" alt="Tutorial" title="Tutorial">
        	<p class="input-top-p admin-top-p">Vimeo Video Link* (Copy the Highlighted Part Only) </p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="379922769" name="register_link" id="register_link" required>           
        </div>
        
        <div class="clear"></div>  -->
        
        <div class="width100 overflow margin-bottom10">
        	<p class="input-top-p admin-top-p">Upload Puppy Photo (Maximum 5)*</p>
			<button type="button" class="btn btn-brand" id="UploadBtn">
                                            Browse
            </button>
            
            <div id="UploadPreview" class="align-items-end ow-uploaded-img"></div>
            
            <input type="file" id="UploadInput"  style="opacity:0;"    multiple data-allow-reorder="true" data-max-files="6">
            
            <!--<p><input id="file-upload" type="file" name="image_one[]" id="image_one" accept="image/*" class="margin-bottom10" multiple /></p>-->

            <!-- <p><input id="file-upload" type="file" name="image_one[]" id="image_two" accept="image/*" class="margin-bottom10" /></p>

            <p><input id="file-upload" type="file" name="image_one[]" id="image_three" accept="image/*" class="margin-bottom10" /></p>

            <p><input id="file-upload" type="file" name="image_one[]" id="image_four" accept="image/*" class="margin-bottom10" /></p>

            <p><input id="file-upload" type="file" name="image_one[]" id="image_five" accept="image/*" class="margin-bottom10" /></p> -->
            <!-- Photo cropping into square size feature -->
        </div>



    
        <div class="clear"></div> 

        <script>
$(function(){
    $("input[type='submit']").click(function(){
        var $fileUpload = $("input[type='file']");
        if (parseInt($fileUpload.get(0).files.length)>6){
         alert("You can only upload a maximum of 6 files");
        }
    });    
});
</script> 
        
    	
        <div class="clear"></div>  

        <div class="width100 overflow text-center">     
          <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" value="submit">Next</button>
        </div>

        </form>
	</div>
</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new Puppy!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to register for Pets !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Fail to register for Puppy !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Puppy name or SKU has been used !! <br> Please enter a new name or SKU !";
        } 
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
<link href="css/croppie.css" rel="stylesheet">
<style>
    #UploadPreview .upload-preview{
        position: relative;
    }
    #UploadPreview .upload-preview.sortable-ghost{
        opacity: .3;
    }
    #UploadPreview .upload-preview .upload-preview-actions-delete{
        display: none;
    }
    #UploadPreview .upload-preview img{
        transition: opacity .3s;
    }
    #UploadPreview .upload-preview.__deleted img{
        opacity: .2;
    }
    #UploadPreview .upload-preview.__deleted .upload-preview-actions{
        display: none;
    }
    #UploadPreview .upload-preview.__deleted .upload-preview-actions-delete{
        display: block;
    }
</style>


<div class="modal" id="UploadModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="CroppieEl" class="croppie-container"><div class="cr-boundary" aria-dropeffect="none" style="width: 500px; height: 500px;"><img class="cr-image" alt="preview" aria-grabbed="false"><div class="cr-viewport cr-vp-square" tabindex="0" style="width: 500px; height: 500px;"></div><div class="cr-overlay"></div></div><div class="cr-slider-wrap"><input class="cr-slider" type="range" step="0.0001" aria-label="zoom"></div></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-brand" id="CroppieBtn">
                    Save
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/tmpl" id="UploadTmpl">
<div class="upload-preview col-md-3 m--margin-top-10" id="Upload_{unique_id}">
    <div class="image-container1">
	<input type="hidden" name="upload[{index}][id]" value="" />
    <input type="hidden" name="upload[{index}][file]" value="{src}" class="upload_file" />
    <input type="hidden" name="upload[{index}][delete]" value="0" class="upload_delete" />
    <img src="{src}" alt="preview" style="width: 100%;" />
	</div>
    <div class="upload-preview-actions m--margin-top-5">
        <a href="#" class="image-edit m-link m-link--state m-link--brand green-a" style="margin: 0 5px;">Edit</a>
        <a href="#" class="image-delete m-link m-link--state m-link--danger red-a" style="margin: 0 5px;">Delete</a>
    </div>
    <div class="upload-preview-actions-delete m--margin-top-5">
        <span class="m--font-danger" style="margin: 0 5px;">DELETED</span>
        <a href="#" class="image-undo m-link m-link--state m-link--success" style="margin: 0 5px;">Undo</a>
    </div>
</div>
</script>

<script type="text/javascript" src="js/sortable.js"></script>
<script type="text/javascript" src="js/croppie.min.js"></script>


<script type="text/javascript">
    
    function copyPetSlugValue() {
        var x = document.getElementById("pet_name").value;
        var y = document.getElementById("pet_sku").value;
        var z = x + '-' + y;

        document.getElementById("slug").value = z.replace(/ /g,"-").toLowerCase();
    }
</script>
                </div>
            </div>
            <!-- end:: Body -->
 
        </div>
        <!-- end:: Page -->

        <!-- end::Scroll Top -->
        <!--begin::Global Theme Bundle -->
        <script src="js/vendors.bundle.js" type="text/javascript"></script>
        <script src="js/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Global Theme Bundle -->
        <!--end::Page Vendors -->

        <script src="js/sortable.js" type="text/javascript"></script>
        <script src="js/croppie.min.js" type="text/javascript"></script>

        <!--end::Page Scripts -->

  
        <!-- End Universal Flash -->
        <!-- Start Summernote Options -->
        <script type="text/javascript">
          

        FilePond.registerPlugin(FilePondPluginImagePreview);
        FilePond.registerPlugin(FilePondPluginFileEncode);
        FilePond.setOptions({
            instantUpload: false,
        });
        const inputElement = document.getElementById('pondInput');
        const pond = FilePond.create( inputElement ,{
            maxFiles: 6,
            allowBrowse: true,
            allowFileEncode:true,
            // server: {
            //     url: '/upload',
            //     process: {
            //         headers: {
            //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //         }
            //     }
            // }
        });

        // $('#createSubmit').click(function(){ //listen for submit event
        //     console.log('hi form did submit' + pond.getFiles());
        //     $("#pondInput").val(function() {
        //             return pond.getFiles();
        //      });
        //     return true;
        // }); 
      

                 // Upload handler
                var $uploadBtn = $('#UploadBtn');
                var $uploadPreview = $('#UploadPreview');
                var $uploadInput = $('#UploadInput');
                var $uploadTmpl = $('#UploadTmpl');
                var $uploadModal = $('#UploadModal');
                
                $uploadBtn.on('click', function() {
                    console.log('did press add images');
                    $uploadInput.trigger('click');
                });

                $uploadInput.on('change', function(e) {
                    var files = this.files;

                    if (files) {
                        for (var i = 0, f; f = files[i]; i++) {
                            readFile(f, function(resp) {
                                var tmpl = $uploadTmpl.html()
                                            .replace(/{src}/g, resp)
                                            .replace(/{index}/g, $uploadPreview.children().length)
                                            .replace(/{unique_id}/g, new Date().getTime());
                                $(tmpl).appendTo($uploadPreview);
                            });
                        }
                    }
                });

                $uploadPreview
                    .on('click', '.image-edit', function(e) {
                        e.preventDefault();
                        var src = $(this).closest('.upload-preview').children('img').attr('src');
                        $croppieTarget = $(this).closest('.upload-preview');

                        $croppie.croppie('bind', {
                            url: src
                        });

                        $uploadModal.modal({
                            keyboard: false
                        });
                    })
                    .on('click', '.image-delete', function(e) {
                        e.preventDefault();
                        $(this).closest('.upload-preview').addClass('__deleted')
                            .children('.upload_delete').val(1);
                    })
                    .on('click', '.image-undo', function(e) {
                        e.preventDefault();
                        $(this).closest('.upload-preview').removeClass('__deleted')
                            .children('.upload_delete').val(0);
                    });


                // croppie
                var $croppieEl = $('#CroppieEl');
                var $croppieBtn = $('#CroppieBtn');
                var $croppieTarget = null;

                var $croppie = $croppieEl.croppie({
                    enableExif: true,
                    enforceBoundary : true,
                    mouseWheelZoom: false,
                    viewport: {
                        width: 500,
                        height: 500
                    },
                    boundary: {
                        width: 500,
                        height: 500
                    }
                });

                $croppieBtn.on('click', function() {
                    if ($croppieTarget) {
                        $croppie.croppie('result', {
                            type: 'base64',
                            size: 'original',
                            quality: 0.8,
                            format: 'jpeg',
                            backgroundColor : "#ffffff"
                        }).then(function (resp) {
                            // update target value
                            $croppieTarget.children('img').attr('src', resp);
                            $croppieTarget.children('.upload_file').val(resp);

                            // hide modal
                            $uploadModal.modal('hide');
                        });
                    }
                });

                // enable sortable
                var sortable = new Sortable($uploadPreview[0], {
                    onEnd: function(e) {
                        // re-order childs index
                        $uploadPreview.children().each(function(index) {
                            $(this).find('input[type=hidden]').each(function() {
                                var name = $(this).attr('name').replace(/\[[0-9]+\]/, '[' + index + ']');
                                $(this).attr('name', name);
                            });
                        });
                    }
                });


                // methods
                function readFile(file, cb) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        cb(e.target.result);
                    };
                    reader.readAsDataURL(file);
                }



    
  
        </script>
   <script src="js/bundle.js" type="text/javascript"></script>
        <script>

// load FilePond resources
var resources = [
    'filepond.css',
    'filepond.js',
    'filepond-plugin-file-encode.min.js',
    'filepond-plugin-file-validate-type.min.js',
    'filepond-plugin-file-validate-size.min.js',
    'filepond-plugin-image-exif-orientation.min.js',
    'filepond-plugin-image-preview.min.css',
    'filepond-plugin-image-preview.min.js',
    'filepond-plugin-image-crop.min.js',
    'filepond-plugin-image-resize.min.js',
    'filepond-plugin-image-transform.min.js',
    'filepond-plugin-image-edit.min.css',
    'filepond-plugin-image-edit.min.js'
].map(function(resource) { return './static/assets/' + resource + '?1588332424238' });

// expose on global scope for demo purposes
window.pond = null;

loadResources(resources).then(function() {

    // register plugins
    FilePond.registerPlugin(
        FilePondPluginFileEncode,
        FilePondPluginFileValidateType,
        FilePondPluginFileValidateSize,
        FilePondPluginImageExifOrientation,
        FilePondPluginImagePreview,
        FilePondPluginImageCrop,
        FilePondPluginImageResize,
        FilePondPluginImageTransform,
        FilePondPluginImageEdit
    );

    // override default options
    FilePond.setOptions({
        dropOnPage: true,
        dropOnElement: true
    });

    // create splash file pond element
    var fields = [].slice.call(document.querySelectorAll('input[type="file"]'));
    var ponds = fields.map(function(field, index) {
        return FilePond.create(field, {
            server: './api/'
        });
    });

    // add warning to multiple files pond
    var pondDemoMultiple = ponds[1];
    var pondMultipleTimeout;
    pondDemoMultiple.onwarning = function() {
        var container = pondDemoMultiple.element.parentNode;
        var error = container.querySelector('p.filepond--warning');
        if (!error) {
            error = document.createElement('p');
            error.className = 'filepond--warning';
            error.textContent = 'The maximum number of files is 3';
            container.appendChild(error);
        }
        requestAnimationFrame(function() {
            error.dataset.state = 'visible';
        });
        clearTimeout(pondMultipleTimeout);
        pondMultipleTimeout = setTimeout(function() {
            error.dataset.state = 'hidden';
        }, 5000);
    };
    pondDemoMultiple.onaddfile = function() {
        clearTimeout(pondMultipleTimeout);
        var container = pondDemoMultiple.element.parentNode;
        var error = container.querySelector('p.filepond--warning');
        if (error) {
            error.dataset.state = 'hidden';
        }
    };

    // set top pond
    pond = ponds[0];

    // show top pond
    pond.element.parentNode.style.opacity = 1;
    setTimeout(function(){
        pond.addFile('./static/assets/filepond.js')
    }, 1250);

    // use developer console to inspect filePond API
    console.log(
        '\n👩🏻‍💻 FilePond API ( window.FilePond ) :\n\n',
        FilePond,
        '\n\n'
    );

    console.log(
        '\n👨🏻‍💻 FilePond instance API ( window.pond ):\n\n',
        pond, 
        '\n\n'
    );

    console.log('\n💬 %cFor development updates → %o\n', 'color: #1da1f2', 'https://twitter.com/rikschennink');

    loadResources(['./static/doka.js']).then(function() {
        
        // assign to multi upload
        var pondMultiple = FilePond.find(document.querySelector('#multi-file-demo .filepond'));
        pondMultiple.imageEditEditor = dokaCreate({
            imagePreviewScaleMode: 'crop',
            cropResizeMatchImageAspectRatio: true,
            cropShowSize: true,
            cropAllowInstructionZoom: true,
            cropResizeKeyCodes: [],
            cropAspectRatioOptions: [
                {
                    label: 'Free',
                    value: null
                },
                {
                    label: 'Portrait',
                    value: 1.5
                },
                {
                    label: 'Square',
                    value: '1:1'
                },
                {
                    label: 'Landscape',
                    value: 0.75
                }
            ]
        });

        // assign to profile picture
        var pondProfile = FilePond.find(document.querySelector('#profile-picture-demo .filepond'));
        pondProfile.imageEditEditor = dokaCreate({
            cropResizeKeyCodes: [],
            utils: ['crop', 'filter', 'color']
        });

    });
});

if ('fetch' in window) {
    (function(){

        function applyStars(stars) {
            document.querySelector('.github-stars span').textContent = (stars + '').replace(/(\d)(?=(\d{3})+(,|$))/g, '$1,');
            sessionStorage.setItem('stars', stars);
        }

        // store in session
        var stars = sessionStorage.getItem('stars');
        if (stars) {
            applyStars(stars);
        }
        else {
            fetch('./stars/')
                .then(function(res) { return res.text() })
                .then(applyStars)
                .catch(function(error) { console.warn(error) });
        }

    }());
}

</script>

</body>
</html>
