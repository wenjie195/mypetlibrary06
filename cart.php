<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Cart.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    addToCart();
    createOrder($conn,$uid);
    header('Location: ./checkout.php');
}

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$orderPending = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' AND shipping_status = 'PENDING' ",array("uid"),array($uid),"s");
// $pendingDetails = $orderPending[0];

$orderShipped = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' AND shipping_status = 'SHIPPING' ",array("uid"),array($uid),"s");
// $shippedDetails = $orderShipped[0];

$orderReceived = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' AND shipping_status = 'DELIVERED' ",array("uid"),array($uid),"s");
// $receivedDetails = $orderReceived[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Cart | Mypetslibrary" />
<title>Cart | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="sticky-tab menu-distance2">
	<div class="tab sticky-tab-tab">
		<button class="tablinks active hover1 tab-tab-btn" onclick="openTab(event, 'Cart')">
            <div class="green-dot"></div>
            <img src="img/cart-1.png" class="tab-icon hover1a" alt="Cart" title="Cart">
            <img src="img/cart-2.png" class="tab-icon hover1b" alt="Cart" title="Cart">
            <p class="tab-tab-p">In Cart</p>
            
        </button>
		<!-- <button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Ship')">
        	<div class="green-dot"></div>
            <img src="img/to-ship-1.png" class="tab-icon hover1a" alt="To Ship" title="To Ship">
            <img src="img/to-ship-2.png" class="tab-icon hover1b" alt="To Ship" title="To Ship">        
            <p class="tab-tab-p">To Ship</p>
        </button>
		<button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Receive')">
        	<div class="green-dot"></div>
            <img src="img/to-receive-1.png" class="tab-icon hover1a" alt="To Receive" title="To Receive">
            <img src="img/to-receive-2.png" class="tab-icon hover1b" alt="To Receive" title="To Receive">         
            <p class="tab-tab-p">To Receive</p>
        </button>
		<button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Received')">
        	<div class="green-dot"></div>
            <img src="img/received-1.png" class="tab-icon hover1a" alt="Received" title="Received">
            <img src="img/received-2.png" class="tab-icon hover1b" alt="Received" title="Received">         
            <p class="tab-tab-p">Received</p>
        </button>         -->

        <a href="shipDeliverReceive.php">
            <button class="tablinks hover1 tab-tab-btn">
                <div class="green-dot"></div>
                <img src="img/to-ship-1.png" class="tab-icon hover1a" alt="To Ship" title="To Ship">
                <img src="img/to-ship-2.png" class="tab-icon hover1b" alt="To Ship" title="To Ship">        
                <p class="tab-tab-p">To Ship</p>
            </button>
        </a>
        <a href="shipDeliverReceive.php">
            <button class="tablinks hover1 tab-tab-btn">
                <div class="green-dot"></div>
                <img src="img/to-receive-1.png" class="tab-icon hover1a" alt="To Receive" title="To Receive">
                <img src="img/to-receive-2.png" class="tab-icon hover1b" alt="To Receive" title="To Receive">         
                <p class="tab-tab-p">To Receive</p>
            </button>
        </a>
        <a href="shipDeliverReceive.php">
            <button class="tablinks hover1 tab-tab-btn">
                <div class="green-dot"></div>
                <img src="img/received-1.png" class="tab-icon hover1a" alt="Received" title="Received">
                <img src="img/received-2.png" class="tab-icon hover1b" alt="Received" title="Received">         
                <p class="tab-tab-p">Received</p>
            </button>  
        </a>


        <!-- <a href="shipDeliverReceive.php">
        	<div class="green-dot"></div>
            <img src="img/to-ship-1.png" class="tab-icon hover1a" alt="To Ship" title="To Ship">
            <img src="img/to-ship-2.png" class="tab-icon hover1b" alt="To Ship" title="To Ship">        
            <p class="tab-tab-p">To Ship</p>
        </a>  
        <a href="shipDeliverReceive.php">
        	<div class="green-dot"></div>
            <img src="img/to-receive-1.png" class="tab-icon hover1a" alt="To Receive" title="To Receive">
            <img src="img/to-receive-2.png" class="tab-icon hover1b" alt="To Receive" title="To Receive">         
            <p class="tab-tab-p">To Receive</p>
        </a>  
        <a href="shipDeliverReceive.php">
        	<div class="green-dot"></div>
            <img src="img/received-1.png" class="tab-icon hover1a" alt="Received" title="Received">
            <img src="img/received-2.png" class="tab-icon hover1b" alt="Received" title="Received">         
            <p class="tab-tab-p">Received</p>
        </a>   -->

	</div>
</div>

<div class="two-menu-space width100"></div>    

<div class="width100 same-padding min-height4 adjust-padding">

    <a href="cartEmpty.php">
        <button class="right-delete clean transparent-button">Delete All</button>
        <div class="clear"></div>
    </a>

	<div  id="Cart" class="tabcontent block same-padding">
        <form method="POST">
            <?php
                $conn = connDB();
                if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
                {
                    $productListHtml = getShoppingCart($conn,2);
                    echo $productListHtml;
                }
                else
                {
                    echo " <h3> YOUR CART IS EMPTY </h3>";
                }


                if(array_key_exists('xclearCart', $_POST))
                {
                    xclearCart();
                }
                else
                {
                // code...
                    unset($productListHtml);
                }

                $conn->close();
            ?>
        </form>
    </div>    

    <div  id="Ship" class="tabcontent same-padding">
        <div class="width100 scroll-div border-separation">
            <table class="green-table width100" id="myTable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Order ID</th>
                        <th>Date</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($orderPending)
                        {
                            for($cnt = 0;$cnt < count($orderPending) ;$cnt++)
                            {
                            ?>    
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $orderPending[$cnt]->getId();?></td>
                                    <td><?php echo $orderPending[$cnt]->getDateCreated();?></td>          
                                    <td>
                                        <!-- <form method="POST" action="pendingPetDetails.php" class="hover1"> -->
                                        <form method="POST" action="#" class="hover1">
                                            <button class="clean hover1 transparent-button pointer" type="submit" name="order_id" value="<?php echo $orderPending[$cnt]->getId();?>">
                                                <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                                <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>    
                </tbody>
            </table>
        </div>
    </div> 

    <div  id="Receive" class="tabcontent same-padding">
        <div class="width100 scroll-div border-separation">
            <table class="green-table width100" id="myTable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Order ID</th>
                        <th>Date</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($orderShipped)
                        {
                            for($cntAA = 0;$cntAA < count($orderShipped) ;$cntAA++)
                            {
                            ?>    
                                <tr>
                                    <td><?php echo ($cntAA+1)?></td>
                                    <td><?php echo $orderShipped[$cntAA]->getId();?></td>
                                    <td><?php echo $orderShipped[$cntAA]->getDateCreated();?></td>          
                                    <td>
                                        <!-- <form method="POST" action="pendingPetDetails.php" class="hover1"> -->
                                        <form method="POST" action="#" class="hover1">
                                            <button class="clean hover1 transparent-button pointer" type="submit" name="order_id" value="<?php echo $orderShipped[$cntAA]->getId();?>">
                                                <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                                <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>    
                </tbody>
            </table>
        </div>    		
    </div>

    <div  id="Received" class="tabcontent same-padding">
        <div class="width100 scroll-div border-separation">
            <table class="green-table width100" id="myTable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Order ID</th>
                        <th>Date</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($orderReceived)
                        {
                            for($cntBB = 0;$cntBB < count($orderReceived) ;$cntBB++)
                            {
                            ?>    
                                <tr>
                                    <td><?php echo ($cntBB+1)?></td>
                                    <td><?php echo $orderReceived[$cntBB]->getId();?></td>
                                    <td><?php echo $orderReceived[$cntBB]->getDateCreated();?></td>          
                                    <td>
                                        <!-- <form method="POST" action="pendingPetDetails.php" class="hover1"> -->
                                        <form method="POST" action="#" class="hover1">
                                            <button class="clean hover1 transparent-button pointer" type="submit" name="order_id" value="<?php echo $orderReceived[$cntBB]->getId();?>">
                                                <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                                <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                    ?>    
                </tbody>
            </table>
        </div> 
    </div>

</div>

<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>

<?php include 'js.php'; ?>

<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>